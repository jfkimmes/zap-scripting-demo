import string
import random
import uuid
from flask import make_response
from flask import render_template

class SessionStateMachine:
    def __init__(self):
        self.session_id = self.__gen_session_id()
        self.csrf_token = self.__gen_session_id()
        self.current_state = 'HOME'
        self.notes: list[Note] = []

        self.graph: dict[tuple[str,str], str] = {('HOME', 'LIST_TODOS'): 'TODO_LIST',
                                                 ('HOME', 'ADD_TODO'): 'TODO_ADD',
                                                 ('TODO_LIST', 'OPEN_TODO'): 'TODO_DETAIL',
                                                 ('TODO_LIST', 'CLOSE'): 'HOME',
                                                 ('TODO_DETAIL', 'CLOSE'): 'TODO_LIST',
                                                 ('TODO_ADD', 'ADD_TODO'): 'HOME',
                                                 ('TODO_ADD', 'CLOSE'): 'HOME',
                                                 }

        self.templates = {'HOME': 'index.html',
                          'TODO_LIST': 'list.html',
                          'TODO_DETAIL': 'detail_ro.html',
                          'TODO_ADD': 'detail.html',
                          }

    def next(self, request_state: str, event: str, request):
        if self.current_state != request_state:
            raise SessionError("Request State false. Logout!")
        if not (self.current_state, event) in self.graph:
            raise SessionError("Invalid state/event pair. Logout!")

        self.current_state = self.graph[(self.current_state, event)]
        
        if self.current_state == 'HOME':
            if event == 'CLOSE':
                response = make_response(
                        render_template(self.templates[self.current_state], currentState=self.current_state))
                response.set_cookie('session', self.session_id)
                return response
            elif event == 'ADD_TODO':
                note = Note(request.form['title'], request.form['text'])
                self.notes.append(note)
                response = make_response(
                        render_template(
                            self.templates[self.current_state], 
                            currentState=self.current_state))
                response.set_cookie('session', self.session_id)
                return response

        elif self.current_state == 'TODO_LIST':
            response = make_response(
                    render_template(self.templates[self.current_state], 
                                    currentState=self.current_state, 
                                    todos=self.notes))
            response.set_cookie('session', self.session_id)
            return response

        elif self.current_state == 'TODO_DETAIL':
            current_note = None
            for todo in self.notes:
                if todo.tid == request.form['tid']:
                    current_note = todo
                    break
            response = make_response(
                    render_template(
                        self.templates[self.current_state],
                        currentState=self.current_state,
                        todo=current_note ))
            response.set_cookie('session', self.session_id)
            return response

        elif self.current_state == 'TODO_ADD':
            response = make_response(
                    render_template(
                        self.templates[self.current_state],
                        currentState=self.current_state))
            response.set_cookie('session', self.session_id)
            return response


    def __gen_session_id(self):
        return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(64))



class SessionError(Exception):
    pass



class Note:
    def __init__(self, title, text):
        self.tid = str(uuid.uuid4())
        self.title = title
        self.text = text

    def __repr__(self):
        return "ToDo Title:" + self.title
