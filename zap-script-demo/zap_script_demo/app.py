from flask import Flask
from flask import request
from flask import render_template
from state import SessionStateMachine, Note, SessionError

app = Flask(__name__)
users: dict[str, SessionStateMachine] = {}

@app.route("/", methods = ["POST", "GET"])
def all_requests():
    error = None

    # bootstrap app with one GET request
    if request.method == "GET":
        return render_template("index.html", currentState="HOME")
    
    # check for exsisting session. Create new otherwise.
    session_cookie = request.cookies.get('session')
    if (not session_cookie or session_cookie not in users):
        current_session = SessionStateMachine()
        users[current_session.session_id] = current_session
    else:
        current_session = users[session_cookie]

    # check necessary app parameters
    if not 'state' in request.form or not 'event' in request.form:
        error = "App state broken. Logout."
        del users[current_session.session_id]
        return render_template('index.html', error=error)
    request_state = request.form['state']
    request_event = request.form['event']

    # render correct view
    try:
        response = current_session.next(request_state, request_event, request)
    except SessionError as e:
        print(e)
        error = "App state broken. Logout."
        del users[current_session.session_id]
        return render_template('index.html', error=error)
    except Exception as e:
        print(e)
        error = "An error occurred :("
        del users[current_session.session_id]
        return render_template('index.html', error=error)
    return response

